from graphene import relay
from graphene_django import DjangoObjectType

from apps.persons.models import Coach, Player


class PlayerType(DjangoObjectType):
    class Meta:
        model = Player
        fields = (
            "first_name",
            "last_name",
            "name",
            "position",
            "shirt_number",
            "nationality",
            "date_of_birth",
            "team",
        )
        filter_fields = "__all__"

        interfaces = (relay.Node,)


class CoachType(DjangoObjectType):
    class Meta:
        model = Coach
        fields = (
            "first_name",
            "last_name",
            "name",
            "nationality",
            "date_of_birth",
            "team",
        )
        filter_fields = "__all__"

        interfaces = (relay.Node,)
