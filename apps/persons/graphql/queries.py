import graphene
from graphene_django.filter import DjangoFilterConnectionField

from apps.leagues.models import League
from apps.persons.graphql.filtersets import CoachFilterSet, PlayerFilterSet
from apps.persons.graphql.types import CoachType, PlayerType
from apps.persons.models import Coach, Player


class QueryException(Exception):
    ...


class PlayerQuery(graphene.ObjectType):
    players = DjangoFilterConnectionField(
        PlayerType,
        filterset_class=PlayerFilterSet,
    )

    def resolve_players(self, info, *args, **kwargs):
        league_code = kwargs.get("league_code")
        if league_code and not League.objects.filter(code=league_code).exists():
            raise QueryException(f"League '{league_code}' doesn't exist in DB.")
        return Player.objects


class CoachQuery(graphene.ObjectType):
    coaches = DjangoFilterConnectionField(
        CoachType,
        filterset_class=CoachFilterSet,
    )

    def resolve_coaches(self, info, *args, **kwargs):
        league_code = kwargs.get("league_code")
        if league_code and not League.objects.filter(code=league_code).exists():
            raise QueryException(f"League '{league_code}' doesn't exist in DB.")
        return Coach.objects


class PersonsQuery(
    PlayerQuery,
    CoachQuery,
):
    ...
