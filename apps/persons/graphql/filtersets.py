import django_filters

from apps.persons.models import Coach, Player


class PlayerFilterSet(django_filters.FilterSet):
    league_code = django_filters.CharFilter(field_name="team__leagues__code")

    class Meta:
        model = Player
        fields = (
            "first_name",
            "last_name",
            "name",
            "position",
            "shirt_number",
            "nationality",
            "league_code",
        )


class CoachFilterSet(django_filters.FilterSet):
    league_code = django_filters.CharFilter(field_name="team__leagues__code")

    class Meta:
        model = Coach
        fields = (
            "first_name",
            "last_name",
            "name",
            "nationality",
            "league_code",
        )
