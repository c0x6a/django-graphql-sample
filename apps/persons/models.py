from django.db import models

from apps.teams.models import Team


class Person(models.Model):
    first_name = models.CharField(
        max_length=128,
        blank=True,
        null=True,
    )
    last_name = models.CharField(
        max_length=128,
        blank=True,
        null=True,
    )
    name = models.CharField(
        max_length=256,
        blank=True,
        null=True,
    )
    date_of_birth = models.DateField(
        blank=True,
        null=True,
    )
    nationality = models.CharField(
        max_length=128,
        blank=True,
        null=True,
    )


class Player(Person):
    position = models.CharField(
        max_length=32,
        blank=True,
        null=True,
    )
    shirt_number = models.IntegerField(
        blank=True,
        null=True,
    )
    team = models.ForeignKey(
        Team,
        related_name="squad",
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING,
    )


class Coach(Person):
    team = models.ForeignKey(
        Team,
        related_name="coach",
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING,
    )
