import json

import httpx
from django.conf import settings

FOOTBALL_DATA_API_URI = settings.FOOTBALL_DATA_API_URI.geturl()

FOOTBALL_API_HEADERS = {
    "X-Auth-Token": settings.FOOTBALL_DATA_API_KEY,
}


class FootballAPIException(Exception):
    ...


def __get_response(endpoint):
    try:
        return httpx.get(
            f"{FOOTBALL_DATA_API_URI}/{endpoint}", headers=FOOTBALL_API_HEADERS
        )
    except Exception as e:
        raise FootballAPIException(e)


def get_league_data(league_code):
    endpoint = f"competitions/{league_code}/"
    response = __get_response(endpoint)
    if response.is_success:
        return response.json()
    else:
        raise FootballAPIException(json.loads(response.content).get("message"))


def get_teams_of_league(league_code):
    endpoint = f"competitions/{league_code}/teams/"
    response = __get_response(endpoint)
    if response.is_success:
        return response.json()
    else:
        raise FootballAPIException(json.loads(response.content).get("message"))


def get_team_coach_and_players(team_code):
    endpoint = f"teams/{team_code}/"
    response = __get_response(endpoint)
    if response.is_success:
        coach = response.json().get("coach")
        players = response.json().get("squad")
        return coach, players
