from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from apps.persons.graphql.types import PlayerType
from apps.teams.models import Team


class TeamType(DjangoObjectType):
    squad = DjangoFilterConnectionField(PlayerType)

    class Meta:
        model = Team
        fields = (
            "name",
            "short_name",
            "tla",
            "area_name",
            "address",
            "leagues",
            "coach",
            "squad",
        )
        filter_fields = (
            "name",
            "short_name",
            "tla",
            "leagues",
        )
        interfaces = (relay.Node,)

    def resolve_squad(self, info, *args, **kwargs):
        return self.squad
