import graphene
from graphene_django.filter import DjangoFilterConnectionField

from apps.teams.graphql.types import TeamType
from apps.teams.models import Team


class TeamQuery(graphene.ObjectType):
    teams = DjangoFilterConnectionField(TeamType)

    def resolve_teams(self, info, *arg, **kwargs):
        return Team.objects
