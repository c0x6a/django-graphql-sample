from django.db import models

from apps.leagues.models import League


class Team(models.Model):
    name = models.CharField(
        max_length=128,
        blank=True,
        null=True,
    )
    short_name = models.CharField(
        max_length=128,
        blank=True,
        null=True,
    )
    tla = models.CharField(
        max_length=6,
        blank=True,
        null=True,
    )
    area_name = models.CharField(
        max_length=128,
        blank=True,
        null=True,
    )
    address = models.CharField(
        max_length=256,
        blank=True,
        null=True,
    )
    leagues = models.ManyToManyField(
        League,
        related_name="teams",
    )

    def __str__(self):
        return self.name
