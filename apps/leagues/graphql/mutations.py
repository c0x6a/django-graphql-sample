import graphene
from django.db import IntegrityError
from apps.utils.football_data_api import (
    get_league_data,
    get_team_coach_and_players,
    get_teams_of_league,
)

from apps.leagues.graphql.types import LeagueType
from apps.leagues.models import League
from apps.persons.models import Coach, Player
from apps.teams.models import Team


class MutationException(Exception):
    ...


class ImportLeagueMutation(graphene.Mutation):
    class Arguments:
        league_code = graphene.String(required=True)

    league = graphene.Field(LeagueType)

    @classmethod
    def mutate(cls, root, info, league_code):
        if League.objects.filter(code=league_code).exists():
            raise MutationException(f"League '{league_code}' already exists in the DB.")

        league_data = get_league_data(league_code)
        league = League.objects.create(
            id=league_data.get("id"),
            code=league_data.get("code"),
            name=league_data.get("name"),
            area_name=league_data.get("area", {}).get("name"),
        )

        league_teams = get_teams_of_league(league_code)
        for team in league_teams.get("teams"):
            if Team.objects.filter(id=team.get("id")).exists():
                team_obj = Team.objects.get(id=team.get("id"))
                team_obj.leagues.add(league)
                continue

            team_data = {
                "id": team.get("id"),
                "name": team.get("name"),
                "short_name": team.get("shortName"),
                "tla": team.get("tla"),
                "area_name": team.get("area", {}).get("name"),
                "address": team.get("address"),
            }
            team_obj = Team.objects.create(**team_data)
            team_obj.leagues.add(league)

            coach = team.get("coach")
            if coach:
                coach_data = {
                    "first_name": coach.get("firstName"),
                    "last_name": coach.get("lastName"),
                    "name": coach.get("name"),
                    "date_of_birth": coach.get("dateOfBirth"),
                    "nationality": coach.get("nationality"),
                    "team": team_obj,
                }
                try:
                    Coach.objects.create(**coach_data)
                except IntegrityError:
                    print(coach_data)

            players = team.get("squad")
            if players:
                for player in players:
                    player_data = {
                        "first_name": player.get("firstName"),
                        "last_name": player.get("lastName"),
                        "name": player.get("name"),
                        "date_of_birth": player.get("dateOfBirth"),
                        "nationality": player.get("nationality"),
                        "position": player.get("position"),
                        "shirt_number": player.get("shirtNumber"),
                        "team": team_obj,
                    }
                    try:
                        Player.objects.create(**player_data)
                    except IntegrityError:
                        print(player_data)
                        continue

        return ImportLeagueMutation(league=league)


class Mutations(graphene.ObjectType):
    import_league = ImportLeagueMutation.Field()
