from graphene import relay
from graphene_django import DjangoObjectType

from apps.leagues.models import League


class LeagueType(DjangoObjectType):
    class Meta:
        model = League
        fields = (
            "code",
            "name",
            "area_name",
            "teams",
        )
        filter_fields = (
            "code",
            "name",
            "area_name",
        )
        interfaces = (relay.Node,)
