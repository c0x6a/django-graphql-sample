import graphene
from graphene_django.filter import DjangoFilterConnectionField

from apps.leagues.graphql.types import LeagueType
from apps.leagues.models import League


class LeagueQuery(graphene.ObjectType):
    leagues = DjangoFilterConnectionField(LeagueType)

    def resolve_leagues(self, info, *args, **kwargs):
        return League.objects
