from django.db import models


class League(models.Model):
    code = models.CharField(max_length=3)
    name = models.CharField(max_length=32)
    area_name = models.CharField(max_length=64)

    def __str__(self):
        return self.code
