#!/usr/bin/env bash

set -euo pipefail
pip install -U pip pip-tools wheel

compile() {
  name=$1
  echo ">>> Compiling $name.in"
  CUSTOM_COMPILE_COMMAND=compile_reqs.sh pip-compile \
    --allow-unsafe \
    --upgrade \
    $name.in \
    -o $name.txt
}

compile "base"
compile "local"
compile "production"
echo ">>> Compiling requirements files done!"
