FROM python:3.9.13-bullseye
ENV PYTHONUNBUFFERED 1
RUN mkdir /api_football
WORKDIR /api_football
RUN mkdir staticfiles
# RUN apt-get update -y && apt-get install -y build-essential libpq-dev
COPY ./requirements /api_football/requirements
RUN pip install -U pip
RUN pip install -r requirements/local.txt
COPY . /api_football
RUN chmod +x /api_football/start
