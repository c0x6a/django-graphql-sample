# Football API Challenge

Back-end Developer Hiring Test, Django + GraphQL

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: BSD

Made with love by: Carlos Joel Delgado Pizarro


## SetUp the project

This project uses Docker, so it's needed to have the following versions installed —at least—: 

- Docker 17.05+.
- Docker Compose 1.17+

The project will create 2 containers, one for the Django application and one for PostgreSQL database, the names of 
the containers will be:

- `api_football_local_django`
- `api_football_local_postgres`

**Why Docker?** As this project will need some libraries to work, and to do some less steps on the process of 
creating a virtual environment, activating it and installing the dependencies manually —well, yes, one command, but 
still you have to write it— using docker helps a lot on reducing those steps and also to distributing the project 
having the same environment and saving some headaches when wanting to run the project in different OSes.

**Why PostgreSQL?** As Django's ORM is made to work with relational data, and the data this project will work on is 
relational, PostgreSQL is the best choice for this scenario —also Django loves PostgreSQL the most than other RDBMS—

Also, I used **Django** as the framework to build this project because it has almost all the things needed 
out-of-the-box to have a web project ready in short time, its ORM is one of the most powerful these days so there's 
no need to write separate SQL queries to access the database, and I added `graphene-django` to work with 
**GraphQL** as it's the most developed library for Django to work with GraphQL implementation.


### Run the project

To run the project, first set the environment variables, there's a file `.env.sample`, just copy/move that to `.
env` file:

`$ cp .env.sample .env`

There are some basic variables already set with default values (no need to change them as it's a local development 
project, but keep in mind that when deploying to production environment, many of those values should be changed), 
from all those values, there's one that it's needed to be set before running the project, `FOOTBALL_DATA_API_KEY` 
should contain a valid API key from https://football-data.org site, so get one and put it in the `.env` file.

Go to the project's root folder and run:

`$ docker-compose up django`

the first time it will build the corresponding containers, install the dependencies of the project, when Docker 
finishes to do its magic, you'll see in the terminal the following lines:

```shell
api_football_local_django | Django version 3.2.14, using settings 'config.settings.local'
api_football_local_django | Development server is running at http://0.0.0.0:8000/
api_football_local_django | Using the Werkzeug debugger (http://werkzeug.pocoo.org/)
api_football_local_django | Quit the server with CONTROL-C.
```

That means that the project is ready to run, you can go to your browser at http://localhost:8000/ and you'll see 
_GraphiQL_ interface ready and waiting for some queries!


## Querying data

### Mutations

Once in _GraphiQL_ interface, first you need to import some data from `football-data` site, there's a mutation for 
that:

```js
mutation {
  importLeague(leagueCode:"CLI") {
    league {
      code
      name
      areaName
    }
  }
}
```

you can change the value of `leagueCode` to the league you want to import.


### Queries

#### Leagues
When you finished importing leagues, you can query them with the following `query`:

```js
query {
  leagues {
    edges {
      node {
        code
        name
        areaName
        teams {
          edges {
            node {
              name
            }
          }
        }
      }
    }
  }
}
```

and you'll see all the leagues that are stored in the local database.

**Note:** all the queries made here will fetch the data from the local database, nothing is requested from 
`football-data`  site anymore.


#### Teams

You can query all the team in the database with:

```js
query {
  teams {
    edges {
      node {
        name
        shortName
        tla
        areaName
        address
        leagues {
          edges {
            node {
              code
              name
            }
          }
        }
        squad {
          edges {
            node {
              name
              position
              dateOfBirth
              nationality
            }
          }
        }
        coach {
          edges {
            node {
              name
              nationality
            }
          }
        }
      }
    }
  }
}
```

Or you can query for a **specific team** —and even list all players with a **specific position**— with:

```js
query {
  teams(name: "CA Boca Juniors") {
    edges {
      node {
        name
        shortName
        tla
        areaName
        address
        leagues {
          edges {
            node {
              code
              name
            }
          }
        }
        squad(position: "Defence") {
          edges {
            node {
              name
              position
              dateOfBirth
              nationality
            }
          }
        }
        coach {
          edges {
            node {
              name
              nationality
            }
          }
        }
      }
    }
  }
}
```

#### Players

You can query all the players in the database with:

```js
query {
  players {
    edges {
      node {
        name
      }
    }
  }
}
```

or filter all player from a specific league

```js
query {
  players(leagueCode: "CLI") {
    edges {
      node {
        name
      }
    }
  }
}
```

or even better, apply some filters if you're searching for a specific player —and show more data in the results if 
you want—

```js
query {
  players(leagueCode: "CLI", name:"Luis Advíncula") {
    edges {
      node {
        name
        position
        team {
          name
        }
      }
    }
  }
}
```

#### Coaches

The same queries from above, also apply for Coaches, just change `players` with `coaches` in the query:

```js
query {
  coaches(leagueCode: "CLI") {
    edges {
      node {
        name
      }
    }
  }
}
```

And there are even more filters and fields you can play with, use _GraphiQL_ autocomplete feature (`Ctrl+Space`) to 
see the fields you can use in each query to show and/or filter the data!
