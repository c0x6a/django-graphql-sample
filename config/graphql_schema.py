import graphene

import apps.leagues.graphql.mutations
import apps.leagues.graphql.queries
import apps.persons.graphql.queries
import apps.teams.graphql.queries


class Query(
    apps.teams.graphql.queries.TeamQuery,
    apps.leagues.graphql.queries.LeagueQuery,
    apps.persons.graphql.queries.PersonsQuery,
):
    ...


class Mutation(
    apps.leagues.graphql.mutations.Mutations,
):
    ...


schema = graphene.Schema(query=Query, mutation=Mutation)
